/*

 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servercontroller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Timer;
import static servercontroller.Server.clients;

/**
 *
 * @author Norbi
 */
public class clientHandler extends Thread{
    
    private Socket socket;
    private Socket sendSocket;
    private Timer timer;
    private Queue<String> msgStack;
    private String socketName = "";
    
    
    public clientHandler(Socket socket){
        this.socket = socket;
        msgStack = new PriorityQueue<>();
    }
    
    
    
    @Override
    public void run(){
        try {
            BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            
            PrintWriter output = new PrintWriter(socket.getOutputStream(),true);
            
            while(true){
            
                String line = input.readLine();
                //output.println(line);
                //System.out.println(line);
               if(!line.contains("|") && line != null){
                    if(!isClientAdded(line)){
                        
                        Server.clients.put(socket, line);
                    }
                }else{
                    
                    System.out.println(line);
                    String[] msg = line.split("\\|");
                    
                    String fromUser = msg[0];
                    String toUser = msg[1];
                    String subject = msg[2];
                    String msgText = msg[3];
                    
                    if(isClientAdded(toUser)){
                        sendMsg(line);
                        if(timer != null){
                            if(timer.isRunning()){
                                timer.stop();
                            }
                        }
                    }else{
                        msgStack.add(line);
                        timer = new Timer(2000, new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                //System.out.println("Üzenet elküldés előtt: "+fromUser+" "+toUser+" "+subject+" "+msgText);
                                System.out.println(msgStack+" "+msgStack.size());
                                if(isClientAdded(toUser)){
                                    if(!msgStack.isEmpty()){
                                        sendMsg(msgStack.poll());
                                        System.out.println("Üzenet: "+fromUser+" "+toUser+" "+subject+" "+msgText);
                                    }
                                    if(msgStack.isEmpty()){
                                        timer.stop();
                                        socketName = "";
                                    }
                                }
                            }
                        });
                        
                        timer.start();
                    }
                }
                
                //System.out.println(Server.clients);
                
                //System.out.println(line);
            }
            
        } catch (SocketException ex) {
            ex.printStackTrace();
            Server.clients.remove(socket);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    
    
    }
    
    public boolean isClientAdded(String clientName){
        //if(socketName != ""){
        //    return true;
        //}
        
        Iterator it = Server.clients.entrySet().iterator();
                        while (it.hasNext()) {
                            Map.Entry pair = (Map.Entry)it.next();
                            //System.out.println(pair.getKey() + " = " + pair.getValue());
                            if(clientName.equals(pair.getValue()+"")){
                                return true;
                            }
                            //it.remove(); 
                    }
        return false;
    }
    
    public void sendMsg(String send){
        System.out.println("MSg meghivva"+" "+send);
        
        String[] msg = send.split("\\|");
                    
        String fromUser = msg[0];
        String toUser = msg[1];
        String subject = msg[2];
        String msgText = msg[3];
        
        Iterator it = Server.clients.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry)it.next();
                //System.out.println(pair.getKey() + " = " + pair.getValue());
                if((pair.getValue()+"").equals(toUser)){
                    
                    try {
                        socket = ((Socket)pair.getKey());
                        //socketName = pair.getValue()+"";
                        //System.out.println(socket+"A socket");
                        PrintWriter output = new PrintWriter(socket.getOutputStream(),true);

                        if(msgText.equals("ap")){
                            output.println(fromUser+"|"+subject+"?$$AP_AD$$");
                        }else{
                            System.out.println(toUser);
                            output.println(fromUser+"|"+subject+"|"+msgText);
                        }
                        System.out.println(pair.getKey() + " = " + pair.getValue());
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                }
                 
             //it.remove(); 
        }
    }
    
}




