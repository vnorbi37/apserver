/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servercontroller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import javax.swing.Timer;
import javafx.util.Pair;

/**
 *
 * @author Norbi
 */
public class Server {
    
    public static volatile HashMap<Socket,String> clients;
    private Timer timer;
    
    public Server(){
        clients = new HashMap<>();
    
    }
    
    
    public void start(){
        try(ServerSocket serverSocket = new ServerSocket(5000)){
            
            while(true){
            
                
                Socket socket = serverSocket.accept();
                System.out.println("Kliens csatlakozott");
                
                new clientHandler(socket).start();
                
                timer = new Timer(2000, new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        System.out.println("Kliensek: "+clients);
                    }
                });
                //timer.start();
                
            }
            
            
        
        }catch(IOException e){
            e.printStackTrace();
        }
            
    }
    
    
    
}
